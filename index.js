// console.log("Hello");

let num = 2;
let exponent = num ** 3
console.log(`The Cube of ${num} is ${exponent} `);

let address = ["Manginginom","Pavia", "Iloilo"];
let [barangay,municipality,province] = address;
console.log(`I live at ${barangay} , ${municipality} , ${province}.`)

let animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: 1075,
	 measurements : {
		feet: 20,
		inches: 3
	}
};

function animalData({name,type,weight,measurements}){
	console.log(`${name} was a ${type}. He weighed at ${weight} kgs with a measurement of ${measurements.feet} ft ${measurements.inches} in.`)
}
animalData(animal);

let Nums = [1,2,3,4,5];


Nums.forEach((Num) =>{
	console.log(Num);
});

let addition = (num1,num2) => num1+num2;
let sum = addition(10,5);
console.log(sum);


class Dog{
	constructor(name,age,breed){
		this.name = name;
		this.age =age;
		this.breed =breed;
	};
};

let myDog = new Dog("Frankie",5, "Miniature Dachschund");
console.log(myDog);